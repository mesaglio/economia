# Economía

## Bibliografia 

* Unidad 1
    * [El problema económico](https://docs.google.com/document/d/1TCJTHfBvqjp0AZN-nI_Qm70XpL8nZUg9xnhMy_fYxNk/edit)
* Unidad 2
    * [Demanda](https://docs.google.com/document/d/1iOYJ8X5b0gTY7ms-tjwdE6aQYKGvQOy5lxAu0LcR-Lw/edit)
    * [Oferta](https://docs.google.com/document/d/15enupzs_t4oyBQC0RDR2x8Z8egidcdEvkv0jE5RAEN0/edit)
    * [Mercados](https://docs.google.com/document/d/1pZk02kWcAoxfZrhL9acsT9YxXN71e0jCts67ESh8gj4/edit)
* Unidad 3
    * [Factores de la producción](https://docs.google.com/document/d/1AWGI4s-yKToPkgMLbKcBXVk9APkjQ94JPTDGEbdujuU/edit)
* Unidad 4
    * [Renta Nacional](https://docs.google.com/document/d/1nLY7M7KiQ_fDMdFZYXajgeoRsI9aouO5NgZE83xO3kY/edit)
* Unidad 5
    * [Sistema Monetario](https://docs.google.com/document/d/1CrC-xAQYl9tbrcl9nRN-4jpgSCukT-miuIHo5Qh41pg/edit#)
* Unidad 6
    * [Depreciacion y amortizacion]( https://docs.google.com/document/d/13kXR0UtlYgK_7ALPhLvq1iWahHNqrJ-pAFbXHvSm0g4/edit)
* Unidad 7
    * [Costos](https://docs.google.com/document/d/1N3zCqmjZCodv01hY4OYPTOQvBjruP925hEpoyJP5Gh4/edit#)
* Unidad 8
    * [Contabilidad y balances](https://docs.google.com/document/d/13a5rluDxEnyOUwC8wSz2zsGpu3AyDd4LkNMVpCbUQfw/edit)
* Unidad 9
    * [Presupuesto de la empresa](https://docs.google.com/document/d/1MBym0UKN_y8n00p8wEkuDWCo8-DVIrqkoiXlvVUUZNc/edit)
* Unidad 10
    * [Matematica financiera](https://docs.google.com/document/d/10BX5PSI-EdzRrC9njcvjF_qiH1EqVxIvSeSbbfXaRwY/edit)
---

## Presentaciones

[Presentación N°01](https://docs.google.com/presentation/d/1p-1u7LM-bEaDD_BbzsffG79jDNOgmiyJeaTVBlVdZhI/edit#slide=id.p3)

[Presentación N°02](https://docs.google.com/presentation/d/10b1V-QaFpBHY0sCuskmEyfZvAgcR4TnPHZDhk8W38_k/edit#slide=id.p3)

[Presentación N°03](https://docs.google.com/presentation/d/1Axgj16Kp7pAikTZ9hH9yVkE8K8VAyAs5KVSCseM_lcU/edit#slide=id.p3)

[Presentación N°04](https://docs.google.com/presentation/d/1zSMk2KP-RQqaqS2VxA-VdqY4vbwkmhoT6r_TCpmNef0/edit#slide=id.p3)

[Presentación N°05](https://docs.google.com/presentation/d/1c4wRtTafmDtNXcbXW-a8Sr1v_VvxrjZ5oTa7RAmHUzA/edit )

[Presentación N°06](https://docs.google.com/presentation/d/1quICJ41UXJ-2-zhGGgvA2T6MMAtiIOKEpueFKLNZZkI/edit#slide=id.p3)

[Presentación N°07](https://docs.google.com/presentation/d/1CEYPGgF8O6t4lM5ni_eyV3JZuw1Vlme6E6op8oNEh6w/edit#slide=id.p4)

[Presentación N°08](https://docs.google.com/presentation/d/14UDb7lR8ABEkPzmp4rlVni5WEW3o4y_HItHCZoDgcns/edit)

[Presentación N°10](https://docs.google.com/presentation/d/1nclzfJTHNnfAEoHdr2rXzcRQM5nxC-SPaSNV5xzKnWs/edit#slide=id.p3)

[Presentación N°09](https://docs.google.com/presentation/d/15WS--fIbRF6S_tKNq8v5EyjIdHNlPGsrhJ1I_cmlWaU/edit#slide=id.p3)

[Presentación N°11](https://docs.google.com/presentation/d/1Nxfq4mOWPX7rqmfZiisTHMB_E8ayMSh93ym7ecEhA4A/edit)

[Presentación N°12](https://docs.google.com/presentation/d/1ypeVjNmiVF_3XeIO2noY19GdLqvmsXb1oAT_UyfSoXU/edit#slide=id.p3)

[Presentación N°13](https://docs.google.com/presentation/d/1blcXX3kFcIzI1Zmd6oLTo8jdNlC_pTtblmuaTp6zXv4/edit#slide=id.p4)

[Presentación N°14](https://docs.google.com/presentation/d/1SOSjluwQFYN-hluwLqca9YX_kA3vJ0P8uigEwUwK8bA/edit)

[Presentación N°16](https://docs.google.com/presentation/d/1o09abYgUJlDXKzg-UQmKYci4zdqYWKvNZkM0HUrmXyk/edit#slide=id.p3)

[Presentación N°15](https://docs.google.com/presentation/d/12hEAvT_FRPNvCnHPGec-snquQiQ1ijpBsVB4MtGjp54/edit)

---

## Trabajo Practico

[Trabajo Práctico N°01](https://docs.google.com/document/d/1itUJNHNuoPRoLnmPZijOWA4-RcyOjWinBD-gDC6z-rw/edit)

[Trabajo Práctico N°02](https://docs.google.com/document/d/13Jt9IqXoFUukFJzp8sNiPemRACnszc16rtEsji9xR_k/edit)

[Trabajo Práctico N°03](https://docs.google.com/document/d/12lHoafOfalMh4IfupsBHL3r8t7lsA3unHEH1IfUmXA0/edit)

[Trabajo Práctico N°04](https://docs.google.com/document/d/1C8yoCDlmLd4CGYjKnLxd78O_rgcn60b_iO-Ou7uHy5I/edit)

[Trabajo Práctico N°05](https://docs.google.com/document/d/1sfb5R-HBo6BHmnfepkaxDqpt9MXLp_a9peGitg2cF28/edit)

[Trabajo Práctico N°06](https://docs.google.com/document/d/1han4Ic-XQ-VbWZrvQkxtJLhuvRV3C74dHu2S6Uc9wdM/edit)

[Trabajo Práctico N°07](https://docs.google.com/document/d/14nxWpg7Ro-KY_pTQTvEO5jQNeCIBLRZIaDXRsZRfRfA/edit)
